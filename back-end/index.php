<?php

include ('database_connection.php');
if (isset($_POST['formsubmitted'])) {
    $error = array();//Declare An Array to store any error message  
    if (empty($_POST['fname'])) {//if no name has been supplied
        $error[] = 'Please Enter a First Name ';//add to array "error"
    } else {
        $fname = $_POST['fname'];//else assign it a variable
    }
    if (empty($_POST['lname'])) {//if no name has been supplied
        $error[] = 'Please Enter a Last Name ';//add to array "error"
    } else {
        $lname = $_POST['lname'];//else assign it a variable
    }

    if (empty($_POST['email'])) {
        $error[] = 'Please Enter your Email ';
    } else {


        if (preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/", $_POST['email'])) {
            //regular expression for email validation
            $email = $_POST['email'];
        } else {
            $error[] = 'Your EMail Address is invalid  ';
        }


    }


    if (empty($_POST['phone'])) {
        $error[] = 'Please Enter a Phone Number ';
    } else {
        $phone = $_POST['phone'];
    }


    if (empty($_POST['password'])) {
        $error[] = 'Please Enter a password ';
    } else {
        $password = $_POST['password'];

    }

    if (empty($_POST['captcha'])) {
        $error[] = 'Please Enter captcha ';
    }else{
        session_start();
        if($_POST['captcha'] != $_SESSION['digit']){
            $error[] = 'Sorry, the CAPTCHA code entered was incorrect';
        }
        session_destroy();
    }



    if (empty($error)) {

        // Make sure the email address is available:
        $query_verify_email = "SELECT * FROM users  WHERE email ='$email'";
        $result_verify_email = mysqli_query($dbc, $query_verify_email);
        if (!$result_verify_email) {
            echo ' Database Error Occured ';
        }

        if (mysqli_num_rows($result_verify_email) == 0) { // IF no previous user is using this email .

            // Create a unique  activation code:
            $activation = md5(uniqid(rand(), true));
            $pass = md5($password);


            $query_insert_user = "INSERT INTO `users` ( `first_name`,`last_name`, `email`, `phone`, `password`, `activation`) VALUES ( '$fname','$lname', '$email', '$phone', '$pass', '$activation')";


            $result_insert_user = mysqli_query($dbc, $query_insert_user);
            if (!$result_insert_user) {
                echo 'Query Failed ';
            }

            if (mysqli_affected_rows($dbc) == 1) { //If the Insert Query was successfull.


                // Send the email:
                $message = " To activate your account, please click on this link:\n\n";
                $message .= WEBSITE_URL . '/activate.php?email=' . urlencode($email) . "&key=$activation";
                mail($email, 'Registration Confirmation', $message, 'From: cristinatest234@gmail.com');

                // Flush the buffered output.

                // Finish the page:
                echo '<div class="success">Thank you for
registering! A confirmation email
has been sent to '.$email.' Please click on the Activation Link to Activate your account </div>';


            } else { // If it did not run OK.
                echo '<div class="errormsgbox">You could not be registered due to a system
error. We apologize for any
inconvenience.</div>';
            }

        } else { // The email address is not available.
            echo '<div class="errormsgbox" >That email
address has already been registered.
</div>';
        }

    } else {//If the "error" array contains error msg , display them



        echo '<div class="errormsgbox"> <ol>';
        foreach ($error as $key => $values) {

            echo '	<li>'.$values.'</li>';



        }
        echo '</ol></div>';

    }

    mysqli_close($dbc);//Close the DB Connection

} // End of the main Submit conditional.



?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Registration Form</title>





    <style type="text/css">
        body {
            font-family:"Lucida Grande", "Lucida Sans Unicode", Verdana, Arial, Helvetica, sans-serif;
            font-size:12px;
        }
        .registration_form {
            margin:0 auto;
            width:500px;
            padding:14px;
        }
        label {
            width: 10em;
            float: left;
            margin-right: 0.5em;
            display: block
        }
        .submit {
            float:right;
        }
        fieldset {
            background:#EBF4FB none repeat scroll 0 0;
            border:2px solid #B7DDF2;
            width: 500px;
        }
        legend {
            color: #fff;
            background: #80D3E2;
            border: 1px solid #781351;
            padding: 2px 6px
        }
        .elements {
            padding:10px;
        }
        p {
            border-bottom:1px solid #B7DDF2;
            color:#666666;
            font-size:11px;
            margin-bottom:20px;
            padding-bottom:10px;
        }
        a{
            color:#0099FF;
            font-weight:bold;
        }

        /* Box Style */


        .success, .warning, .errormsgbox, .validation {
            border: 1px solid;
            margin: 0 auto;
            padding:10px 5px 10px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
            font-weight:bold;
            width:450px;

        }

        .success {

            color: #4F8A10;
            background-color: #DFF2BF;
            background-image:url('images/success.png');
        }
        .warning {

            color: #9F6000;
            background-color: #FEEFB3;
            background-image: url('images/warning.png');
        }
        .errormsgbox {

            color: #D8000C;
            background-color: #FFBABA;
            background-image: url('images/error.png');

        }
        .validation {

            color: #D63301;
            background-color: #FFCCBA;
            background-image: url('images/error.png');
        }



    </style>

</head>
<body>


<form action="index.php" method="post" class="registration_form" onsubmit="return checkForm(this);">
    <fieldset>
        <legend>Registration Form </legend>

        <p>Create A new Account <span style="background:#EAEAEA none repeat scroll 0 0;line-height:1;margin-left:210px;;padding:5px 7px;">Already a member? <a href="login.php">Log in</a></span> </p>

        <div class="elements">
            <label for="fname">First Name :</label>
            <input type="text" id="fname" name="fname" size="25" value="<?php if($fname){ echo $fname; }else{ echo $_POST['fname']; } ?>" />
        </div>
        <div class="elements">
            <label for="lname">Last Name :</label>
            <input type="text" id="lname" name="lname" size="25" value="<?php if($lname){ echo $lname; }else{ echo $_POST['lname']; } ?>" />
        </div>
        <div class="elements">
            <label for="phone">Phone:</label>
            <input type="text" id="phone" name="phone" size="25" pattern="[0-9]{10}" value="<?php if($phone){ echo $phone; }else{ echo $_POST['phone']; } ?>" />
        </div>
        <div class="elements">
            <label for="email">E-mail :</label>
            <input type="text" id="email" name="email" size="25" value="<?php if($email){ echo $email; }else{ echo $_POST['email']; } ?>" />
        </div>
        <div class="elements">
            <label for="password">Password :</label>
            <input type="password" id="password" name="password" size="25" pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$" />
        </div>


        <div class="elements">
            <label>Captcha :</label>
            <input type="text" id="captcha" name="captcha" size="25" />
            <p><img src="captcha.php" width="120" height="30" border="1" alt="CAPTCHA"></p>

        </div>

        <div class="submit">
            <input type="hidden" name="formsubmitted" value="TRUE" />
            <input type="submit" value="Register" />
        </div>
    </fieldset>
</form>
<script type="text/javascript">

    function checkForm(form)
    {


        if(!form.captcha.value.match(/^\d{5}$/)) {
            alert('Please enter the CAPTCHA digits in the box provided');
            form.captcha.focus();
            return false;
        }

        return true;
    }

</script>

</body>
</html>
