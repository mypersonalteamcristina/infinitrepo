$(document).ready(function () {

    $('#myform').validate({ // initialize the plugin
        rules: {
            name: {
                required: true,
                minlength: 3
            },
            company: {
                required: true,
                minlength: 3
            },

            email: {
                required: true,
                email: true
            },
            phone: {
                required: true,
                minlength: 10
            }
        },

        submitHandler: function (form) { // for demo
            form.submit();

        }
    });




});